package main

import (
  "log"
  "time"
  "net/http"
  "encoding/json"

  "github.com/gorilla/mux"
)


func main() {
  router := mux.NewRouter().StrictSlash(true)
  router.HandleFunc("/time", Index)

  

  log.Fatal(http.ListenAndServe(":8000", router))
}


func Index(w http.ResponseWriter, r *http.Request) {
  type timeywimey struct {
    Name  string
    Value  time.Time
  }

  wibblywobbly := timeywimey{
    Name: "Time",
    Value: time.Now(),
  }

  json.NewEncoder(w).Encode(wibblywobbly)
}
